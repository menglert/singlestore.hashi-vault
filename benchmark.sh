#!/bin/sh

sed -E "s#VAULT_ADDR#$VAULT_ADDR#g" /opt/vault-benchmark/configs/config.hcl > /tmp/config.hcl
cat /tmp/config.hcl > /opt/vault-benchmark/configs/config.hcl
rm /tmp/config.hcl
sed -E "s#VAULT_DEV_ROOT_TOKEN_ID#$VAULT_DEV_ROOT_TOKEN_ID#g" /opt/vault-benchmark/configs/config.hcl > /tmp/config.hcl
cat /tmp/config.hcl > /opt/vault-benchmark/configs/config.hcl
rm /tmp/config.hcl
sed -E "s#VAULT_NAMESPACE#$VAULT_NAMESPACE#g" /opt/vault-benchmark/configs/config.hcl > /tmp/config.hcl
cat /tmp/config.hcl > /opt/vault-benchmark/configs/config.hcl
rm /tmp/config.hcl
sed -E "s#ROOT_PASSWORD#$ROOT_PASSWORD#g" /opt/vault-benchmark/configs/config.hcl > /tmp/config.hcl
cat /tmp/config.hcl > /opt/vault-benchmark/configs/config.hcl
rm /tmp/config.hcl

vault-benchmark run -config=/opt/vault-benchmark/configs/config.hcl