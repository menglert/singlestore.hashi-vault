# HashiCorp Vault Database Secrets Integration with SingleStore
## Pre-Requisites
* docker engine
* docker-compose
* SingleStore (S2) License

## Run Benchmark
* Clone/Download this repository e.g. ```git clone <REPO_URL>```
* Setup ```.env``` file
  * Copy ```env.sample``` to ```.env```
  * Set ```ROOT_PASSWORD``` and ```SINGLESTORE_LICENSE```
* Build and run the Containers ```docker-compose up --force-recreate --build```
* The Bnechmark should take around 1.5 minutes to complete. Verify the output:
```
vault-benchmark-1  | Target: http://vault:8200
vault-benchmark-1  | op         count  rate        throughput  mean         95th%         99th%         successRatio
vault-benchmark-1  | s2_test_1  11881  197.896166  196.332540  50.722533ms  183.856626ms  560.599314ms  100.00%
```
* Done. You can destroy the containers by pressing ```CTRL+C```

## Run Manually
* Clone/Download this repository e.g. ```git clone <REPO_URL>```
* Setup ```.env``` file
  * Copy ```env.sample``` to ```.env```
  * Set ```ROOT_PASSWORD``` and ```SINGLESTORE_LICENSE```
* Build and run the Containers ```docker-compose up --force-recreate --build```
* Give the Containers a couple of seconds to start up. You should see the something similar like this:
```
s2-1     | 08704957 2024-03-06 06:59:42.462   INFO: Thread 100000 (ntid 254, conn id 8): SetLicenseV4: Build Hash = <REDACTED>
s2-1     | 08705001 2024-03-06 06:59:42.462   INFO: Thread 100000 (ntid 254, conn id 8): SetLicenseV4: Expiration = unlimited
s2-1     | 08705012 2024-03-06 06:59:42.462   INFO: Thread 100000 (ntid 254, conn id 8): SetLicenseV4: Capacity = 4 units
s2-1     | 08705023 2024-03-06 06:59:42.462   INFO: Thread 100000 (ntid 254, conn id 8): SetLicenseV4: Options = 40 (Free edition)
```
* Run the test script ```docker exec hashi-vault-vault-1 /test_s2_mysql_plugin.sh```
* Validate the Log Output:
```
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                myroot
token_accessor       <REDACTED>
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
Success! Enabled the database secrets engine at: database/
Success! Data written to: database/config/my-mysql-database
Success! Data written to: database/roles/my-role
Key                Value
---                -----
lease_id           database/creds/my-role/<REDACTED>
lease_duration     1h
lease_renewable    true
password           <REDACTED>
username           <REDACTED>
```
* Double check within S2
  * Connect using cmdlet ```docker exec -ti hashi-vault-s2-1 singlestore -p<REDACTED>```
  * Change to information_schema DB ```use information_schema;```
  * Query the Users ```select USER from USERS;```
  * Validate the created Users:
```
+----------------------------------+
| USER                             |
+----------------------------------+
| v-token-my-role-<REDACTED>       |
| root                             |
+----------------------------------+
2 rows in set (0.01 sec)
```
* Done. You can destroy the containers by pressing ```CTRL+C```
