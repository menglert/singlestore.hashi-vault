#!/bin/sh

vault login -non-interactive token=$VAULT_DEV_ROOT_TOKEN_ID
vault secrets enable database
vault write database/config/my-mysql-database \
    plugin_name=mysql-database-plugin \
    connection_url="root:$ROOT_PASSWORD@tcp(database:3306)/" \
    allowed_roles="my-role" \
    username="vaultuser" \
    password="vaultpass"
vault write database/roles/my-role \
    db_name=my-mysql-database \
    creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT SELECT ON *.* TO '{{name}}'@'%';" \
    default_ttl="1h" \
    max_ttl="24h"
vault read database/creds/my-role