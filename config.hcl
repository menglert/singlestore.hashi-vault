vault_addr = "VAULT_ADDR"
vault_token = "VAULT_DEV_ROOT_TOKEN_ID"
vault_namespace = "VAULT_NAMESPACE"
duration = "60s"
report_mode = "terse"
random_mounts = true
cleanup = true

test "mysql_secret" "s2_test_1" {
    weight = 100
    config {
        db_connection {
            connection_url = "{{username}}:{{password}}@tcp(s2:3306)/"
            username = "root"
            password = "ROOT_PASSWORD"
        }

        role {
            creation_statements = "CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT SELECT ON *.* TO '{{name}}'@'%';"
            default_ttl = "10s"
        }
    }
}
